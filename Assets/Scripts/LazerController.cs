﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerController : MonoBehaviour {

    public static LazerController instance;

    public Rigidbody body;
    public float speed=20;


    private void Awake()
    {
        if (instance == null) { instance = this; }
    }
            
    void Start () {
        body = GetComponent<Rigidbody>();      
    }
		
	public void Pew (Vector3 forward) {
        transform.forward = forward;
        body.velocity = forward * 210;
        StartCoroutine(PewCoroutine()); 
    }
     
    IEnumerator PewCoroutine() { 
        yield return new WaitForSeconds(1.5f);
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision c)
    {
        

        if (c.gameObject.tag == "Enemy" || c.gameObject.tag=="Destroyer")
        {
            c.gameObject.SetActive(false);
            gameObject.SetActive(false);
            GameManager.instance.explode.transform.position = gameObject.transform.position;
            GameManager.instance.explode.Play();
            AudioManager.instance.PlaySFX("Boom");
            
        }
        else { gameObject.SetActive(false); }
    }

}
