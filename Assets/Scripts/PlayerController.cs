﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public static PlayerController instance;

    Animator anim;
    public Rigidbody body;
    public float speed = 10;

    public float boostSpeed = 21;
    public float normalSpeed = 7;
    public float brakeSpeed = 4;
    private float forward = 7;
    public GameObject gun1;
    public GameObject gun2;
    public Transform gun1t;
    public Transform gun2t;
    public Transform gun3t;
    public bool powered=false;
    public bool modeSwitch = false;
    public float energy=100;
    public bool flip=false;
    public bool flop = false;
    
   

    private void Awake()
    {
        if (!instance) { instance = this; }
        forward = normalSpeed;
    }
    
    void Start()
    {
        anim = GetComponent<Animator>();
        body = GetComponent<Rigidbody>();        
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        anim.SetFloat("rightVelocity", x);
        anim.SetFloat("upVelocity", y);


        if (GameManager.instance.gameLock)
        {
            gameObject.transform.position = new Vector3(0, 4, 0);
            body.velocity = new Vector3(x, y, forward) * 0;
        }
        if (!GameManager.instance.gameLock)
        {
            if (!modeSwitch)
            {
                body.velocity = new Vector3(x*2.25f, y*1.25f, forward) * speed;
            }

            if (modeSwitch)
            {
                transform.eulerAngles += new Vector3(-y,x,0).normalized;

                body.velocity= transform.forward * forward*speed/1.5f;               
            }
            

            if (Input.GetButtonDown("Jump"))
            {
                if (!powered)
                {
                    GameObject lazer = Spawner.instance.Spawn("Lazer");
                    lazer.transform.position = gun1t.position;
                    lazer.GetComponent<LazerController>().Pew(gun1t.forward);

                    AudioManager.instance.PlaySFX("lazerPew");
                }

                if (powered)
                {
                    GameObject lazer = Spawner.instance.Spawn("Lazer");
                    lazer.transform.position = gun2t.position;
                    lazer.GetComponent<LazerController>().Pew(gun2t.forward);

                    GameObject lazer2 = Spawner.instance.Spawn("Lazer");
                    lazer2.transform.position = gun3t.position;
                    lazer2.GetComponent<LazerController>().Pew(gun3t.forward);

                    AudioManager.instance.PlaySFX("lazerPew");
                }
            }
            if (Input.GetButtonDown("Fire3"))
            {
                if (!flip)
                {
                    StartCoroutine(Wheee());
                }
            }

            if (Input.GetButtonDown("Fire1"))
            {
                if (!flop)
                {
                    StartCoroutine(Brake());
                }
            }

            if (Input.GetButtonDown("Bbutton"))
            {if (GameManager.instance.bombNumber > 0)
                {
                    GameObject bomb = Spawner.instance.Spawn("Bomb");
                    bomb.transform.position = gun1t.position;
                    bomb.GetComponent<BombController>().Zoom(gun1t.forward);
                    GameManager.instance.bombNumber--;
                    AudioManager.instance.PlaySFX("BombShoot");
                }
            }

            
        }
        
    }







    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == ("Enemy"))
        {
            GameManager.instance.health -= 20;
            other.gameObject.SetActive(false);
            
            GameManager.instance.explode.transform.position = other.gameObject.transform.position;
            GameManager.instance.explode.Play();
            AudioManager.instance.PlaySFX("Boom");
        }

        if (other.gameObject.tag == ("Turret"))
        {
            GameManager.instance.health -= 50;   
        }

        if (other.gameObject.tag == ("Cube"))
        {            
            GameManager.instance.health -= 10;
            
            GameManager.instance.ow.transform.position = other.transform.position;
             other.gameObject.SetActive(false);
            AudioManager.instance.PlaySFX("Ouch");
            GameManager.instance.ow.Play();
            
        }

        if (other.gameObject.tag == ("Destroyer"))
        {
            GameManager.instance.health -= 20;           
            other.gameObject.SetActive(false);
            GameManager.instance.explode.transform.position = other.gameObject.transform.position;
            GameManager.instance.explode.Play();
            AudioManager.instance.PlaySFX("Boom");
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == ("TheEnd"))
        {
            GameManager.instance.gameLock = true;
            AudioManager.instance.PlaySFX("Death");
        }

        if (other.gameObject.tag == ("Ring"))
        {
            AudioManager.instance.PlaySFX("Ding");
            GameManager.instance.score += 1;
        }

        if (other.gameObject.tag == ("LazerPUp"))
        {
            AudioManager.instance.PlaySFX("Pickup");
            powered = true;
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == ("BombPUp"))
        {
            GameManager.instance.bombNumber++;
            AudioManager.instance.PlaySFX("Pickup");
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.tag == ("TTrigger"))
        {
            EnemyController.instance.TTrigger = true;            
        }

        if (other.gameObject.tag == ("RTrigger"))
        {
           modeSwitch = true;
            AudioManager.instance.PlaySFX("RangeT");
            GameManager.instance.RangeTrigger.SetActive(false);
            GameManager.instance.SouthWall.SetActive(true);
        }

        if (other.gameObject.tag == ("RangeWall"))
        {
            StartCoroutine(UTurn());            
        }

        if (other.gameObject.tag == ("ATrigger"))
        {
            GameManager.instance.menuText.text = "How do you people keep doing this";
        }


    }
    IEnumerator Wheee()
    {
        flip = true;
        for (float t = 0; t < 0.3f; t += Time.deltaTime) {
            float frac = t / 0.3f;
            forward = Mathf.Lerp(normalSpeed, boostSpeed, frac);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1);
        for (float t = 0; t < 0.5f; t += Time.deltaTime)
        {
            float frac = t / 0.5f;
            forward = Mathf.Lerp(boostSpeed, normalSpeed, frac);
            yield return new WaitForEndOfFrame();
        }
        flip = false;   
    }

    IEnumerator Brake()
    {
        flop = true;
        for (float t = 0; t < 0.3f; t += Time.deltaTime)
        {
            float frac = t / 0.3f;
            forward = Mathf.Lerp(normalSpeed, brakeSpeed, frac);
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1);
        for (float t = 0; t < 0.5f; t += Time.deltaTime)
        {
            float frac = t / 0.5f;
            forward = Mathf.Lerp(brakeSpeed, normalSpeed, frac);
            yield return new WaitForEndOfFrame();
        }
        flop = false;
    }

    
    IEnumerator UTurn()
    {
        while (enabled)
        {
            transform.LookAt(GameManager.instance.LookIt);
            AudioManager.instance.PlaySFX("UTurn");
            yield return new WaitForSeconds(.5f);
            yield break;
        }
    }
}
