﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour {

	
	
	// Update is called once per frame
	void Update () {

        if (gameObject.tag == "LazerPUp" || gameObject.tag == "BombPUp")
        {
            transform.Rotate(0, -50 * Time.deltaTime, 0);
        }

        else if (gameObject.tag == "Spinner")
        {
            transform.Rotate(-50 * Time.deltaTime, 0, 0);
        }

        else 
        {
            transform.Rotate(50 * Time.deltaTime, 0, 0);
        }
    }
}
