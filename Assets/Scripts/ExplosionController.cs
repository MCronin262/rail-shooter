﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour {
    


    private void OnEnable()
    {
        StartCoroutine(Vanish());
        
    }

    void Update()
    {
        transform.Rotate(200 * Time.deltaTime, -200 * Time.deltaTime, 0);
        transform.localScale += new Vector3(0.07F, 0.07F, 0.07F);
    }
    private void OnCollisionEnter(Collision c)
    {


        if (c.gameObject.tag == "Enemy" || c.gameObject.tag == "Destroyer" || c.gameObject.tag == "Turret")
        {
            c.gameObject.SetActive(false);
            
            GameManager.instance.explode.transform.position = c.gameObject.transform.position;
            GameManager.instance.explode.Play();
            AudioManager.instance.PlaySFX("Boom");

        }
    }


        IEnumerator Vanish()
    {
        yield return new WaitForSeconds(0.5f);
        transform.localScale = new Vector3(1, 1, 1);
        gameObject.SetActive(false);
    }
}
