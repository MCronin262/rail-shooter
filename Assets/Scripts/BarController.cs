﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarController : MonoBehaviour {


    public static BarController instance;
    public RectTransform energyB;
    private float energy=100;
    private float full = 100;
    private float empty = 0;
    private float timer;
    public Color eC;
    public Color fullC;
    public Color emptyC;
    public Image eImage;
    public RectTransform healthBar;
    public Color hC;
    public Color healthy;
    public Color okay;
    public Color hurt;
    public Image hImage;
    private float fullHealth = 100;
    private float batman;

    private void Awake()
    {
        if (!instance) { instance = this; }
       // eImage = GetComponent<Image>();
    }


       
   void Update ()
   {
        if (Input.GetButtonDown("Fire3"))
        {
            StartCoroutine(Hail());
        }

        if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(Unicorn());
        }

        energyB.sizeDelta = new Vector2(energy * 2.5f, energyB.sizeDelta.y);
        eImage.color = eC;

        batman = GameManager.instance.health / fullHealth;
        healthBar.sizeDelta = new Vector2(GameManager.instance.health * 2.5f, healthBar.sizeDelta.y);
        hImage.color = hC;


        if (GameManager.instance.health >= 50)
        {
            hC = Color.Lerp(okay, healthy, batman);
        }

        else 
        {
            hC = Color.Lerp(hurt, okay, batman);
        }
    }


    IEnumerator Hail ()
    {        
            for (float t = 0; t < 1.3f; t += Time.deltaTime)
            {
                float frac = t / 1.3f;
                energy = Mathf.Lerp(full, empty, frac);
                eC = Color.Lerp(fullC, emptyC, frac);
                yield return new WaitForEndOfFrame();
            }
            
            for (float t = 0; t < 0.5f; t += Time.deltaTime)
            {
                float frac = t / 0.5f;
                energy = Mathf.Lerp(empty, full, frac);
                eC = Color.Lerp(emptyC, fullC, frac);
                yield return new WaitForEndOfFrame();
            }
    }

    IEnumerator Unicorn()
    {
        for (float t = 0; t < 1.3f; t += Time.deltaTime)
        {
            float frac = t / 1.3f;
            energy = Mathf.Lerp(full, empty, frac);
            eC = Color.Lerp(fullC, emptyC, frac);
            yield return new WaitForEndOfFrame();
        }

        for (float t = 0; t < 0.5f; t += Time.deltaTime)
        {
            float frac = t / 0.5f;
            energy = Mathf.Lerp(empty, full, frac);
            eC = Color.Lerp(emptyC, fullC, frac);
            yield return new WaitForEndOfFrame();
        }
    }

}

