﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour {
    public static BombController instance;
    public Rigidbody body;
    public float speed = 30;

    private void Awake()
    {
        if (instance == null) { instance = this; }
    }



    // Use this for initialization
    void Start ()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, -600 * Time.deltaTime, 0);
       
    }

    public void Zoom(Vector3 forward)
    {
        transform.forward = forward;
        body.velocity = forward * 180;
        StartCoroutine(ZoomCoroutine());
    }

    IEnumerator ZoomCoroutine()
    {
        yield return new WaitForSeconds(1f);
        GameObject Explosion = Spawner.instance.Spawn("Explosion");
        AudioManager.instance.PlaySFX("BombExplode");
        Explosion.transform.position = gameObject.transform.position;
        yield return new WaitForSeconds(.01f);
        gameObject.SetActive(false);
    }

    

    private void OnCollisionEnter(Collision c)
    { StartCoroutine(Wham()); }


       
    

    IEnumerator Wham()
    {

        yield return new WaitForSeconds(.001f);
        GameObject Explosion = Spawner.instance.Spawn("Explosion");
        AudioManager.instance.PlaySFX("BombExplode");
        Explosion.transform.position = gameObject.transform.position;
        yield return new WaitForSeconds(.001f);
        gameObject.SetActive(false);
    }

}

