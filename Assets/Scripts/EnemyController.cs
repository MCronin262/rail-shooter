﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public static EnemyController instance;

    public GameObject missile;
    public Transform center;
    public GameObject central;
    public bool TTrigger = false;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        if (gameObject.tag == "Turret")
        {
            StartCoroutine(MissileShoot());
        }
    }
    

    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag == "Enemy")
        {
            transform.Rotate(0, 50 * Time.deltaTime, 0);
        }

        if (GameManager.instance.gameLock)
        {
            gameObject.SetActive(true);
        }


        if (gameObject.tag == "Destroyer")
        {
            transform.Rotate(0, 30 * Time.deltaTime, 0);

            
        }

    }

    public void Resurrect()
    {
        Debug.Log("Yo!");
        if (gameObject.tag == "Enemy")
        {
            gameObject.SetActive(true);
        }
        
    }

    IEnumerator MissileShoot()
    {
         while (enabled)
         {
            yield return new WaitForSeconds(4);
            GameObject missile = Spawner.instance.Spawn("Missile");
            missile.transform.position = center.position;
         }
    }



  

}
