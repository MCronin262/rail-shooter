﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {


    public static GameManager instance;

    public int score=0;
    public Text hiScoreText;
    public ParticleSystem explode;
    public ParticleSystem ow;
    public bool gameLock=true;
    public bool startScreen = true;
    public Text menuText;
    public GameObject menuImage;
    public GameObject controls;
    public AudioSource music;
    public AudioSource menuMusic;
    public Text scoreText;
    public Text smallText;
    public Text bombText;
    public Text healthText;
    public int health = 100;
    public int bombNumber = 0;
    public GameObject RangeTrigger;
    public GameObject SouthWall;    
    public Transform LookIt;   
    public bool zoom = false;
    public Text prompt;
    public GameObject healthB2;
    public GameObject healthB3;
    public GameObject energyB1;
    public GameObject energyB2;

    private void Awake()
    {
        if (instance == null) { instance = this; }
    }
    
    void Start () {
        SouthWall.SetActive(false);
        controls.SetActive(false);        
    }	
	
	void Update () {

        if (gameLock&&startScreen)
        {
            menuText.text = "AT GALAXY'S EDGE";
            smallText.text = "Press Space to Start";
            bombText.text = "";
            scoreText.text = "";
            healthText.text = "";
            menuImage.SetActive(false);
            healthB2.SetActive(false);
            healthB3.SetActive(false);
            energyB1.SetActive(false);
            energyB2.SetActive(false);

            if (!menuMusic.isPlaying) { menuMusic.Play(); }

            if (Input.GetButtonDown("Zbutton"))
            {
                controls.SetActive(true);
            }

            if (Input.GetButtonUp("Zbutton"))
            {                 
                controls.SetActive(false);                
            }

            if (Input.GetButtonDown("Jump"))
            {
                
                menuText.text = "";
                smallText.text = "";
                prompt.text = "";
                healthB2.SetActive(true);
                healthB3.SetActive(true);
                energyB1.SetActive(true);
                energyB2.SetActive(true);
                menuImage.SetActive(true);
                gameLock = false;
                startScreen = false;
                score = 0;
                menuMusic.Stop();
                music.Play();

            }
        }

        if (gameLock && !startScreen)
        {
            menuText.text = "Training over. Try again?";
            bombText.text = "";
            scoreText.text = "Score: " + score;
            healthText.text = "";
            healthB2.SetActive(false);
            healthB3.SetActive(false);
            energyB1.SetActive(false);
            energyB2.SetActive(false);
            menuImage.SetActive(false);
            music.Stop();
            if (!menuMusic.isPlaying) menuMusic.Play();

            if (Input.GetButtonDown("Jump"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
            }
        }


        if (!gameLock && !startScreen)
        {
            bombText.text = bombNumber + " Bombs";
            scoreText.text = "Score: " + score;
            healthText.text = "Health: "+ health;
        }

        if (health <= 0)
        {
            gameLock = true;
            AudioManager.instance.PlaySFX("Death");
        }

        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        if (score > highScore)
        {
            PlayerPrefs.SetInt("HighScore", score);
            PlayerPrefs.Save();
            hiScoreText.text = "High Score: " + highScore;
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
      
    }
    
}

