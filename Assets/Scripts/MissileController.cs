﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

    public static MissileController instance;

    public Rigidbody body;
    public float speed = 2;
    public Transform target;
    public float deeps = 0.5f;


    private void Awake()
    {
        if (instance == null) { instance = this; }
    }
    
    void Start()
    {
        body = GetComponent<Rigidbody>();
        transform.rotation = Quaternion.LookRotation(PlayerController.instance.transform.position);
    }

    void Update()
    {
        StartCoroutine(Fwoosh());

        if (!EnemyController.instance.TTrigger) { gameObject.SetActive(false); }

       
        Vector3 diffp = (PlayerController.instance.gun1t.transform.position + (PlayerController.instance.body.velocity*deeps)) - transform.position;

        body.velocity = diffp.normalized * speed;

        if (GameManager.instance.gameLock)
        {
            gameObject.SetActive(false);
        }
    }    

    IEnumerator Fwoosh()
    {

        while (enabled)
        {            
            yield return new WaitForSeconds(0.4f);
            StartCoroutine(Vanish());            
        }
    }

    IEnumerator Vanish()
    {
        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
    }
}
